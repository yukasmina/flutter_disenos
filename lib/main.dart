import 'package:disenioflutter/src/page/botones_page.dart';
import 'package:flutter/material.dart';
import 'package:disenioflutter/src/page/basic_page.dart';
import 'package:disenioflutter/src/page/scroll_page.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Diseño',
      initialRoute: 'botones',
      routes: {
        'basico':(BuildContext context)=> BasicPage(),
        'scroll':(BuildContext context)=> ScrollPage(),
        'botones':(BuildContext context)=> BotonesPage(),
      },
      home: BasicPage(),
    );
  }
}
